<footer>
  <div class="container">
    <div class="row">

      <div class="col-sm-3">
        <h5>Shalom Creations</h5>
        <p><small>Address for Sneha's Secret Hideout</small></p>
      </div>

      <div class="col-sm-3">
        <h5>More</h5>
        <ul class="list-unstyled">
          <li><a href="#soon">Blog</a></li>
          <li><a href="#soon">Fashion Tips</a></li>
        </ul>
      </div>

      <div class="col-sm-3">
        <h5>Connect</h5>
        <ul class="list-unstyled">
          <li><a href="#soon">Facebook</a></li>
          <li><a href="#soon">Twitter</a></li>
          <li><a href="#soon">Pinterest</a></li>
        </ul>
      </div>

    </div>
    <hr>
    <br/>
  </div>
</footer>

</body></html>